import React, { Component } from 'react';
import { connect } from 'react-redux';

class Add extends Component {

  handleClick() {
    const product = this.props.product;
    this.props.getCurrentId(product);
  }

  render() {

    return (
      <div>
        <button onClick={this.handleClick.bind(this)}>点击添加</button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  addOrder: state.add.addOrder
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addProduct
}, dispatch);


export default Add