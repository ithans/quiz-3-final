export const getAllProducts = () => (dispatch) => {
  fetch('http://localhost:8080/api/product')
    .then(response =>response.json())
    .then(result => {
      console.log(result);
      dispatch({
        type:'GET_ALL_PRODUCTS',
        productsList:result
      })
    })
};