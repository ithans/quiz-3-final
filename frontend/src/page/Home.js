import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllProducts } from "../action/homeAction";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import Add from "../component/Add";
class Home extends Component {
  
  componentDidMount() {
    this.props.getAllProducts();
  }
  render() {
    const productsList = this.props.productsList;
    return (
      <div>
        {productsList.map((value,index) => {
          return (
            <div className='list'>
              <img src="../img/{value.url}" />
                {value.name}
                单价：{value.price}/{value.unit}<br/>
                <Add product={value} /> 
            </div>
          )
        })}
        
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productsList:state.products.productsList
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllProducts
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Home);