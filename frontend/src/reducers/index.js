import {combineReducers} from "redux";
import homeReducer from "../reducer/homeReducer";
import addReducer from "../reducer/addReducer";

const reducers = combineReducers({
  products:homeReducer,
  add:addReducer
});
export default reducers;