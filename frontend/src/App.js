import React, {Component} from 'react';
import './App.less';
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router";
import Home from "./page/Home";

class App extends Component{
  render() {
    return (
      <div className='App'>
        Hello World
        <Router>
          <Switch>
            <Route path={"/"} component={Home}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;