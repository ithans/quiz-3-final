const initState = {
  productsList: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_ALL_PRODUCTS':
      return {
        ...state,
        productsList: action.productsList
      };

    default:
      return state
  }
};