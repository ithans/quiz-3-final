const initState = {
  addOrder: false
};
export default (state = initState, action) => {
  switch (action.type) {
    case 'ADD_ORDER':
      return {
        ...state,
        addOrder: action.addOrder
      };
    default:
      return state
  }
};