package com.thoughtworks.twuc.webApp;


import com.thoughtworks.twuc.webApp.domain.Product;
import com.thoughtworks.twuc.webApp.domain.ProductRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ShopTest extends ApiTestBase {
    @Autowired
    private ProductRepo productRepo;
    @Test
    void should_post_product() throws Exception {
        mockMvc.perform(post("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"kele\",\"price\":3,\"url\":\"nuu\",\"unit\":\"bottle\"}"))
                .andExpect(status().isCreated());
    }

    @Test
    void should_not_post_product() throws Exception {
        mockMvc.perform(post("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"price\":3,\"url\":\"nuu\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_get_all_product() throws Exception {
        productRepo.save(new Product("kele",2.99,"qqq","bottle"));
        mockMvc.perform(get("/api/product"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"name\":\"kele\",\"price\":2.99,\"url\":\"qqq\",\"unit\":\"bottle\"}]"));
    }

}
