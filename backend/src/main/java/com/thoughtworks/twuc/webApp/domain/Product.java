package com.thoughtworks.twuc.webApp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false,length = 64)
    @NotNull
    @Size(max = 64)
    private String name;
    @Column(nullable = false)
    @NotNull
    private Double price;
    @Column(nullable = false)
    @NotNull
    private String url;
    @NotNull
    @Column(nullable = false)
    private String unit;
    public String getUnit() {
        return unit;
    }

    public Product() {
    }

    public Product(@NotNull @Size(max = 64) String name, @NotNull Double price, @NotNull String url, @NotNull String unit) {
        this.name = name;
        this.price = price;
        this.url = url;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUrl() {
        return url;
    }
}
