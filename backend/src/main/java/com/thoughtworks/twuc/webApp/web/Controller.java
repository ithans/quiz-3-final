package com.thoughtworks.twuc.webApp.web;

import com.thoughtworks.twuc.webApp.domain.Product;
import com.thoughtworks.twuc.webApp.domain.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Random;

/**
 * @author bo.wang
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(value = {"*"})
public class Controller {
    @Autowired
    private ProductRepo productRepo;
    @PostMapping("/product")
    public ResponseEntity saveProduct(@RequestBody @Valid Product product){
        Product product1 = productRepo.save(product);
        if (product1==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else{
            return ResponseEntity.status(HttpStatus.CREATED)
                    .contentType(MediaType.APPLICATION_JSON)
                    .build();
        }
    }

    @GetMapping("/product")
    public ResponseEntity<List> getAllProduct(){
        List<Product> products = productRepo.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .body(products);
    }


}
