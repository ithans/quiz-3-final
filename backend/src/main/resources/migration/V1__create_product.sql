create table product(
    id int not null ,
    name varchar(64) not null ,
    price double not null ,
    unit varchar(20) not null ,
    url varchar(128) not null,
    primary key (id)
)